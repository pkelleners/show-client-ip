package com.gitlab.pkelleners.showclientip;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;


@RestController
public class HelloController {

    private static final String[] IP_HEADERS = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR",
            "X-Envoy-External-Address"
    };

    @GetMapping("/")
    public String getClientIp(HttpServletRequest request) {

        StringBuilder body = new StringBuilder();

        body.append("RemoteAddress:").append(request.getRemoteAddr()).append("<br>");
        for (String header : IP_HEADERS) {
            Enumeration<String> o = request.getHeaders(header);
            body.append(header).append(" : ");
            while (o.hasMoreElements()) {
                body.append(o.nextElement());
                body.append(" ,");
            }
            body.append("<br>");
        }

        return body.toString();
    }

}