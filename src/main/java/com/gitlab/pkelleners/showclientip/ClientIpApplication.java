package com.gitlab.pkelleners.showclientip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientIpApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientIpApplication.class, args);
    }

}
